# README #

這組程式庫是我開發 MT4 程式以及相關工具, 以下為各個程式的簡單介紹

### MQL4 ###
此目錄下的程式為 MQL4 程式, 可複製到 MT4 環境中編譯並執行

* Experts\Manual Trade Helper: 手動操作小幫手
* Experts\TEA No.5: 雙包寧傑通道交易策略
* Experts\TEA No.6: 鱷魚線交易策略(在某次台北團聚中分享的簡單 EA 程式)
* Experts\TEA No.7: 海龜交易策略
* Indicators\AverageTrueRange.mq4: ATR 指標, 提供 TR, ATR 及 TR 標準差等數據
* Indicators\Donchain.mq4: Donchain 通道指標 (海龜交易策略須配合 Donchain 指標運作)
* Indicators\OpenInterest: OI 籌碼面指標
* Scripts\Delete All Objects: 刪除畫面上所有的繪圖物件, 如文字, 線條, 按鈕 ...
* Scripts\Draw High Low: 繪製指定範圍內的高低點及四分位線
* Scripts\Draw Order History: 將交易歷史軌跡繪製在圖表上
* Scripts\Export Trade Records: 將交易記錄匯出為 CSV 檔
* Scripts\Mimic Dog: 手動下單並模擬為英雄社團的狗狗程式, 適合手動干預時使用
* Scripts\Reset Take Profit: 重新設定目前持倉的停利價格
* Scripts\Download CME OI: 由芝加哥交易所下載期貨未平倉資料
* Include\\*: 上述程式會使用到的共用函數
* Files\\*: 上述程式會使用到的設定檔

### utilities ###
此目錄下為 MT4 程式相關的工具

* mt4-launcher\\*: 自動以 portable 模式啟動 MT4 Terminal 的指令
* forextools\\*: 一組指令列工具, 包括 OI 資料下載, 交易紀錄管理, 回測交易資料轉換...