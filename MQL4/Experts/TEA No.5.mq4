#property copyright "Copyright 2016-2017, Tim Hsu"
#property link      ""
#property version   "1.2"
#property description "根據雙包寧傑通道判斷進出場時機，自動進行佈局"
#property strict
#include <TEA.mqh>


//使用者輸入參數
input string CUSTOM_COMMENT             = "【提姆茶５號 v1.2】";     //畫面註解
input int    TRACE_PREV_BARS            = 60;                        //向前回溯幾根 K 棒以標示入場點
input string BREAK_LINE_1               = "＝＝＝＝＝";              //＝ [ 進場控制 ] ＝＝＝＝＝＝
input string TRADE_DAYS                 = "123456";                  //操作日 (星期 0123456)
input int    TRADE_START_HOUR           = 0;                         //操作開始時間
input int    TRADE_END_HOUR             = 23;                        //操作結束時間
input string BREAK_LINE_2               = "＝＝＝＝＝";              //＝ [ 下單參數 ] ＝＝＝＝＝＝
input double INITIAL_LOTS               = 1;                         //下單手數
const double MULTIPLIER                 = 1;                         //加碼比例
const int    MAXIMUM_LOTS               = 3;                         //最大手數
const double MINIMUM_LOTS               = 0.01;                      //最小手數
const int    MAXIMUM_ORDERS             = 1;                         //最大單數
input int    TAKE_PROFIT                = 0;                         //獲利點數 (0: 自動判斷)
input int    STOP_LOSS                  = 0;                         //停損點數 (0: 自動判斷)
input string BREAK_LINE_4               = "＝＝＝＝＝";              //＝ [ 交易紀錄 ] ＝＝＝＝＝＝
input bool   EXPORT_TXN_LOG             = true;                      //每日匯出交易紀錄


//EA 相關
const int    MAGIC_NUMBER         = 930214;
const string ORDER_COMMENT_PREFIX = "TEA5_";    //交易單說明前置字串
const int    ARROW_GAP            = 15;         //畫 signal 箭號距 K 棒上下端的點數


//資訊顯示用的 Label 物件名稱
const string LBL_COMMENT         = "lblComment";
const string LBL_TRADE_ENV       = "lblTradEvn";
const string LBL_PRICE           = "lblPrice";
const string LBL_SIGNAL_TEXT     = "lblTrendingText";
const string LBL_SIGNAL          = "lblSignal";
const string LBL_SPREAD          = "lblSpread";
const string LBL_SERVER_TIME     = "lblServerTime";
const string LBL_LOCAL_TIME      = "lblLocalTime";
const string LBL_TRADE_TIME      = "lblTradeTime";
const string LBL_STOP_TRADE      = "lblStopTrade";
const string LBL_BASIC_PARAM     = "lblBasicParam";
const string LBL_TAKE_PROFIT     = "lblTakeProfit";
const string LBL_STOP_LOSS       = "lblStopLoss";
const string LBL_MAX_LOSS_AMT    = "lblMaxLossAmt";
const string ARROW_UP            = "↑";
const string ARROW_DOWN          = "↓";
const string ARROW_NONE          = "＝";
const string TRADE_TIME_MSG      = "茶棧已打烊，明日請早！";
const string STOP_TRADE_MSG      = "已達停損標準，茶棧暫停營業！";
const string BTN_SET_BUY_SIGNAL  = "btnSetBuySignal";
const string BTN_SET_SELL_SIGNAL = "btnSetSellSignal";
const string BTN_SET_NONE_SIGNAL = "btnSetNoneSignal";
const string LN_CHECK_LINE       = "lnCheckLine";
const string LN_TARGET_LINE      = "lnTargetLine";


//全域變數
static bool        gs_isTradeTime   = false;
static bool        gs_stopTrading   = false;
static string      gs_symbol        = Symbol();
static long        gs_chartId       = 0;
static int         gs_lastSignal    = SIGNAL_NONE;
static string      gs_fileName      = "TEA5_" + (string)AccountNumber() + ".txt";
/*
static double      gs_maxLossAmt    = 0;
static string      gs_maxLossAmtKey = "MaxLossAmount";
*/
static string      gs_lastExportDate = "";
static OrderStruct gs_buyPosition[];
static OrderStruct gs_sellPosition[];
static double      gs_basePrice = 0;   //基準價位
static int         gs_slDistance = 0;  //停損距離
static double      gs_checkPoint = 0;  //檢核價位
static double      gs_targetPrice = 0; //出場價位

int OnInit() {
    _logger = CLog4mql::getInstance();
    
    gs_symbol = Symbol();
    gs_chartId = ChartID();
    gs_isTradeTime = IsTradeTime(TRADE_DAYS, TRADE_START_HOUR, TRADE_END_HOUR);
    gs_lastSignal = GetSignalByDBB();
    gs_lastExportDate = "";
    gs_basePrice = 0;
    gs_slDistance = 0;
    gs_checkPoint = 0;
    gs_targetPrice = 0;
        
    CollectOrders(gs_symbol, OP_BUY, MAGIC_NUMBER, gs_buyPosition);
    CollectOrders(gs_symbol, OP_SELL, MAGIC_NUMBER, gs_sellPosition);
    _logger.debug(__FILE__, __LINE__, StringFormat("Current position: BUY = %d, SELL = %d", ArraySize(gs_buyPosition), ArraySize(gs_sellPosition)));

    ResetCheckPoint();

    PutInfoLables();
    UpdateInfoLabels();
    UpdateSignalLabel();
    SetTradeTimeLabel(gs_isTradeTime);

    return INIT_SUCCEEDED;
}


void OnDeinit(const int reason) {
    ObjectsDeleteAll(gs_chartId);
    _logger.release();
}


void OnTick() {
    UpdateInfoLabels();

    if(ArraySize(gs_buyPosition) > 0 && TAKE_PROFIT == 0 && STOP_LOSS == 0) {
        if(Ask > gs_checkPoint + (Ask - Bid))  ResetCheckPoint();
        if(Bid < gs_targetPrice) {
            _logger.debug(__FILE__, __LINE__, StringFormat("Current price %.5f lower than target price %.5f, closing BUY orders...", Bid, gs_targetPrice));
            CloseMarketOrders(gs_buyPosition);
            CollectOrders(gs_symbol, OP_BUY, MAGIC_NUMBER, gs_buyPosition);
        }
    }
        
    if(ArraySize(gs_sellPosition) > 0 && TAKE_PROFIT == 0 && STOP_LOSS == 0) {
        if(Bid < gs_checkPoint - (Ask - Bid))  ResetCheckPoint();
        if(Ask > gs_targetPrice) {
            _logger.debug(__FILE__, __LINE__, StringFormat("Current price %.5f higher than target price %.5f, closing SELL orders...", Ask, gs_targetPrice));
            CloseMarketOrders(gs_sellPosition);
            CollectOrders(gs_symbol, OP_SELL, MAGIC_NUMBER, gs_sellPosition);
        }
    }    
    
    /*
    if(AccountProfit() < gs_maxLossAmt) {
        gs_maxLossAmt = NormalizeDouble(AccountProfit(), 2);
        WriteData(gs_fileName, gs_maxLossAmtKey, StringFormat("%.2f", MathAbs(gs_maxLossAmt)));
        PrintFormat("Max loss amount reached $%.2f", MathAbs(gs_maxLossAmt));
    }
    */

    if(!HasNewBar())  return;

    //export orders closed in previous day
    if(EXPORT_TXN_LOG && TimeToString(TimeCurrent(), TIME_DATE) != gs_lastExportDate) {
        ExportTradeHistory(gs_symbol, "", "", MAGIC_NUMBER);
        gs_lastExportDate = TimeToString(TimeCurrent(), TIME_DATE);
    }

    //refresh current position    
    CollectOrders(gs_symbol, OP_BUY, MAGIC_NUMBER, gs_buyPosition);
    CollectOrders(gs_symbol, OP_SELL, MAGIC_NUMBER, gs_sellPosition);

    //checking signal availability, and then close position (hopfully profitable)
    if(gs_lastSignal == SIGNAL_BUY && Close[1] < GetBollingerBand(Period(), 1, MODE_UPPER, 1)) {
        gs_lastSignal = SIGNAL_NONE;
        if(ArraySize(gs_buyPosition) > 0) {
            _logger.debug(__FILE__, __LINE__, "BUY signal dismissed, closing BUY position...");
            CloseMarketOrders(gs_buyPosition);
            CollectOrders(gs_symbol, OP_BUY, MAGIC_NUMBER, gs_buyPosition);
        }
    }
    
    if(gs_lastSignal == SIGNAL_SELL && Close[1] > GetBollingerBand(Period(), 1, MODE_LOWER, 1)) {
        gs_lastSignal = SIGNAL_NONE;
        if(ArraySize(gs_sellPosition) > 0) {
            _logger.debug(__FILE__, __LINE__, "SELL signal dismissed, closing SELL position...");
            CloseMarketOrders(gs_sellPosition);
            CollectOrders(gs_symbol, OP_SELL, MAGIC_NUMBER, gs_sellPosition);
        }
    }


    //check if signal appears
    int signal = GetSignalByDBB();
    if(signal != SIGNAL_NONE && signal != gs_lastSignal)  {
        gs_lastSignal = signal;
    }
    if(signal == SIGNAL_BUY) {
        PutSignalArrow(SIGNAL_BUY, Time[1], Low[1] - ARROW_GAP * Point);
    } else if(signal == SIGNAL_SELL) {
        PutSignalArrow(SIGNAL_SELL, Time[1], High[1] + ARROW_GAP * Point);
    }
    UpdateSignalLabel();
    
    //check trading time
    gs_isTradeTime = IsTradeTime(TRADE_DAYS, TRADE_START_HOUR, TRADE_END_HOUR);
    SetTradeTimeLabel(gs_isTradeTime);
    if(!gs_isTradeTime)  return;

    if(gs_stopTrading)  return;

    //目前只做一單
    if(gs_lastSignal == SIGNAL_BUY && ArraySize(gs_buyPosition) == 0) {
        _logger.debug(__FILE__, __LINE__, "BUY signal found, placeing buy order...");
        PlaceOrder(gs_buyPosition, OP_BUY);
    }

    if(gs_lastSignal == SIGNAL_SELL && ArraySize(gs_sellPosition) == 0) {
        _logger.debug(__FILE__, __LINE__, "SELL signal found, placeing sell order...");
        PlaceOrder(gs_sellPosition, OP_SELL);
    }

    _logger.debug(__FILE__, __LINE__, StringFormat("Current position: BUY = %d, SELL = %d", ArraySize(gs_buyPosition), ArraySize(gs_sellPosition)));
}


void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam) {
    if(sparam == BTN_SET_NONE_SIGNAL) {
        gs_lastSignal = SIGNAL_NONE;
        UpdateSignalLabel();
    }

    if(sparam == BTN_SET_BUY_SIGNAL) {
        gs_lastSignal = SIGNAL_BUY;
        UpdateSignalLabel();
    }

    if(sparam == BTN_SET_SELL_SIGNAL) {
        gs_lastSignal = SIGNAL_SELL;
        UpdateSignalLabel();
    }

    ObjectSetInteger(gs_chartId, sparam, OBJPROP_STATE, false);
}


//下單邏輯
void PlaceOrder(OrderStruct& orders[], int orderType) {
    int orderCnt = ArraySize(orders);
    if(orderCnt >= MAXIMUM_ORDERS) {
        _logger.info(__FILE__, __LINE__, "Reached maximum orders, REJECT not place new order.");
        return;
    }
    
    double lastLots = (orderCnt == 0)? 0 : orders[orderCnt - 1].lots;
    double lots = 0;
    double currentPrice = 0;
    double takeProfit = 0;
    double stopLoss = 0;
    string comment;
    string orderTypeString = "";
    
    if(orderType == OP_BUY) {
        orderTypeString = "BUY";
        currentPrice = Ask;
        takeProfit = (TAKE_PROFIT == 0)? 0 : currentPrice + TAKE_PROFIT * Point();
        stopLoss = (STOP_LOSS == 0)? 0: currentPrice - STOP_LOSS * Point();
        gs_slDistance = PriceToInteger(currentPrice - Low[1]);
        if(currentPrice > GetBollingerBand(Period(), 2, MODE_UPPER, 1, PRICE_CLOSE))  // cross over bb2 upper
            gs_slDistance = (int)MathRound(gs_slDistance / 2);
        
    } else {
        orderTypeString = "SELL";
        currentPrice = Bid;
        takeProfit = (TAKE_PROFIT == 0)? 0 : currentPrice - TAKE_PROFIT * Point();
        stopLoss = (STOP_LOSS == 0)? 0 : currentPrice + STOP_LOSS * Point();
        gs_slDistance = PriceToInteger(High[1] - currentPrice);
        if(currentPrice < GetBollingerBand(Period(), 2, MODE_LOWER, 1, PRICE_CLOSE))  // cross over bb2 lower
            gs_slDistance = (int)MathRound(gs_slDistance / 2);
    }
    comment = BuildOrderComment(orderType, orderCnt + 1) + "_" + (string)gs_slDistance;

    if(orderCnt == 0) {
        lots = INITIAL_LOTS;
    }
    else {
        lots = NormalizeDouble(lastLots * MULTIPLIER, 2);
        if(lots > MAXIMUM_LOTS)  lots = MAXIMUM_LOTS;
        if(lots < MINIMUM_LOTS)  lots = MINIMUM_LOTS;
    }
    
    _logger.debug(__FILE__, __LINE__, StringFormat("%s price: %.5f, tp: %.5f, sl: %.5f, slDistantce: %d", orderTypeString, currentPrice, takeProfit, stopLoss, gs_slDistance));
    int ticket = SendOrder(gs_symbol, orderType, currentPrice, lots,comment, MAGIC_NUMBER, takeProfit, stopLoss);
    
    if(ticket > 0) {
        AddTicketToPosition(ticket, orders);
        ResetCheckPoint();
    }
}


//重設各項追蹤點位
void ResetCheckPoint() {
    if(TAKE_PROFIT > 0 || STOP_LOSS > 0)  return;
    
    string tmp[];

    if(ArraySize(gs_buyPosition) > 0) {
        StringSplit(gs_buyPosition[ArraySize(gs_buyPosition) - 1].comment, StringGetCharacter("_", 0), tmp);
        gs_slDistance = (int)StringToInteger(tmp[ArraySize(tmp) - 1]);
        if(gs_basePrice == 0)  gs_basePrice = gs_buyPosition[ArraySize(gs_buyPosition) - 1].openPrice;
        if(gs_checkPoint > 0)  gs_basePrice = gs_checkPoint;
        gs_targetPrice = gs_basePrice - gs_slDistance * Point();
        gs_checkPoint = gs_basePrice + gs_slDistance * Point();

    } else if(ArraySize(gs_sellPosition) > 0) {
        StringSplit(gs_sellPosition[ArraySize(gs_sellPosition) - 1].comment, StringGetCharacter("_", 0), tmp);
        gs_slDistance = (int)StringToInteger(tmp[ArraySize(tmp) - 1]);
        if(gs_basePrice == 0)  gs_basePrice = gs_sellPosition[ArraySize(gs_sellPosition) - 1].openPrice;
        if(gs_checkPoint > 0)  gs_basePrice = gs_checkPoint;
        gs_targetPrice = gs_basePrice - gs_slDistance * Point();
        gs_checkPoint = gs_basePrice + gs_slDistance * Point();

    } else {
        gs_basePrice = 0;
        gs_slDistance = 0;
        gs_targetPrice = 0;
        gs_checkPoint = 0;
    }
    DrawTradeLines();
    _logger.debug(__FILE__, __LINE__, StringFormat("Reset Target: %.5f, Base: %.5f, Check point: %.5f, SL distance: %d", gs_targetPrice, gs_basePrice, gs_checkPoint, gs_slDistance));
}


//put signal arrow
void PutSignalArrow(int signalType, datetime arrowTime, double arrowPrice) {
    const int BUY_ARROW_CODE  = 233;
    const int SELL_ARROW_CODE = 234;
    
    static int arrowId = 0;
    string arrowName = "SIGNAL_" + (string)arrowId++;
    color arrowColor;
    
    if(signalType == SIGNAL_BUY) {
        arrowColor = clrBlue;
        
        ObjectCreate(gs_chartId, arrowName, OBJ_ARROW, 0, arrowTime, arrowPrice);
        ObjectSetInteger(gs_chartId, arrowName, OBJPROP_ARROWCODE, BUY_ARROW_CODE);
        ObjectSetInteger(gs_chartId, arrowName, OBJPROP_COLOR, arrowColor);
    }

    if(signalType == SIGNAL_SELL) {
        arrowColor = clrRed;
        
        ObjectCreate(gs_chartId, arrowName, OBJ_ARROW, 0, arrowTime, arrowPrice);
        ObjectSetInteger(gs_chartId, arrowName, OBJPROP_ARROWCODE, SELL_ARROW_CODE);
        ObjectSetInteger(gs_chartId, arrowName, OBJPROP_COLOR, arrowColor);
    }
}


//交易單註解
string BuildOrderComment(int orderType, int orderSeq) {
    if(orderType == OP_BUY)
        return ORDER_COMMENT_PREFIX + gs_symbol + "_B-" + (string)orderSeq;

    if(orderType == OP_SELL)
        return ORDER_COMMENT_PREFIX + gs_symbol + "_S-" + (string)orderSeq;
    
    return NULL;    
}


//在圖表上安置各項資訊標籤物件
void PutInfoLables() {
    //put signal arrows for past K bars
    int signal = SIGNAL_NONE;
    for(int i = 1; i <= TRACE_PREV_BARS; i++) {
        signal = GetSignalByDBB(i);
        if(signal == SIGNAL_BUY) {
            PutSignalArrow(SIGNAL_BUY, Time[i], Low[i] - ARROW_GAP * Point);
        } else if(signal == SIGNAL_SELL) {
            PutSignalArrow(SIGNAL_SELL, Time[i], High[i] + ARROW_GAP * Point);
        }
    }

    //comment label
    ObjectCreate(gs_chartId, LBL_COMMENT, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_YDISTANCE, 24);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_COLOR, clrYellow);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_FONTSIZE, 12);
    ObjectSetString(gs_chartId, LBL_COMMENT, OBJPROP_FONT, "微軟正黑體");
    string custComment = CUSTOM_COMMENT;
    ENUM_ACCOUNT_TRADE_MODE accountType = (ENUM_ACCOUNT_TRADE_MODE)AccountInfoInteger(ACCOUNT_TRADE_MODE);
    switch(accountType) {
        case ACCOUNT_TRADE_MODE_DEMO: 
            custComment += "模擬倉"; 
            break; 
        case ACCOUNT_TRADE_MODE_REAL: 
            custComment += "真倉"; 
            break; 
        default: 
            break; 
    } 
    SetLabelText(gs_chartId, LBL_COMMENT, custComment);

    //交易品種及時區
    ObjectCreate(gs_chartId, LBL_TRADE_ENV, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_YDISTANCE, 45);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_COLOR, clrOrange);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_FONTSIZE, 18);
    ObjectSetString(gs_chartId, LBL_TRADE_ENV, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_TRADE_ENV, Symbol() + "-" + GetTimeFrameString(Period()));

    //價格
    ObjectCreate(gs_chartId, LBL_PRICE, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_YDISTANCE, 72);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDeepSkyBlue);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_FONTSIZE, 24);
    ObjectSetString(gs_chartId, LBL_PRICE, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_PRICE, StringFormat("%." + (string)Digits + "f", Close[0]));

    //進場訊號標題
    ObjectCreate(gs_chartId, LBL_SIGNAL_TEXT, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL_TEXT, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL_TEXT, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL_TEXT, OBJPROP_XDISTANCE, 82);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL_TEXT, OBJPROP_YDISTANCE, 98);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL_TEXT, OBJPROP_COLOR, clrNavajoWhite);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL_TEXT, OBJPROP_FONTSIZE, 12);
    ObjectSetString(gs_chartId, LBL_SIGNAL_TEXT, OBJPROP_FONT, "微軟正黑體");
    SetLabelText(gs_chartId, LBL_SIGNAL_TEXT, "進場信號：");

    //進場訊號
    ObjectCreate(gs_chartId, LBL_SIGNAL, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_XDISTANCE, 70);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_YDISTANCE, 98);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_COLOR, clrLightGray);
    ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_FONTSIZE, 12);
    ObjectSetString(gs_chartId, LBL_SIGNAL, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_SIGNAL, ARROW_NONE);

    //點差
    ObjectCreate(gs_chartId, LBL_SPREAD, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_SPREAD, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_SPREAD, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_SPREAD, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_SPREAD, OBJPROP_YDISTANCE, 98);
    ObjectSetInteger(gs_chartId, LBL_SPREAD, OBJPROP_COLOR, clrNavajoWhite);
    ObjectSetInteger(gs_chartId, LBL_SPREAD, OBJPROP_FONTSIZE, 12);
    ObjectSetString(gs_chartId, LBL_SPREAD, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_SPREAD, StringFormat("(%.0f)", MarketInfo(gs_symbol, MODE_SPREAD)));

    //主機時間
    ObjectCreate(gs_chartId, LBL_SERVER_TIME, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_CORNER, CORNER_RIGHT_LOWER);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_YDISTANCE, 30);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_COLOR, clrLimeGreen);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_FONTSIZE, 10);
    ObjectSetString(gs_chartId, LBL_SERVER_TIME, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_SERVER_TIME, "主機：" + TimeToString(TimeCurrent(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));

    //本機時間
    ObjectCreate(gs_chartId, LBL_LOCAL_TIME, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_CORNER, CORNER_RIGHT_LOWER);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_YDISTANCE, 15);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_COLOR, clrLimeGreen);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_FONTSIZE, 10);
    ObjectSetString(gs_chartId, LBL_LOCAL_TIME, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_LOCAL_TIME, "本地：" + TimeToString(TimeLocal(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));

    /*
    //set none signal button
    ObjectCreate(gs_chartId, BTN_SET_NONE_SIGNAL, OBJ_BUTTON, 0, 0, 0);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_CORNER, CORNER_LEFT_LOWER);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_XDISTANCE, 10);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_YDISTANCE, 140);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_XSIZE, 130);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_YSIZE, 30);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_COLOR, clrBlack);
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_BGCOLOR, C'236,233,216');
    ObjectSetInteger(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_FONTSIZE, 10);
    ObjectSetString(gs_chartId, BTN_SET_NONE_SIGNAL, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, BTN_SET_NONE_SIGNAL, "Set NONE Signal");

    //set buy signal button
    ObjectCreate(gs_chartId, BTN_SET_BUY_SIGNAL, OBJ_BUTTON, 0, 0, 0);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_CORNER, CORNER_LEFT_LOWER);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_XDISTANCE, 10);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_YDISTANCE, 105);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_XSIZE, 130);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_YSIZE, 30);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_COLOR, clrBlue);
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_BGCOLOR, C'236,233,216');
    ObjectSetInteger(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_FONTSIZE, 10);
    ObjectSetString(gs_chartId, BTN_SET_BUY_SIGNAL, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, BTN_SET_BUY_SIGNAL, "Set BUY Signal");

    //set sell signal button
    ObjectCreate(gs_chartId, BTN_SET_SELL_SIGNAL, OBJ_BUTTON, 0, 0, 0);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_CORNER, CORNER_LEFT_LOWER);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_XDISTANCE, 10);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_YDISTANCE, 70);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_XSIZE, 130);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_YSIZE, 30);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_COLOR, clrRed);
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_BGCOLOR, C'236,233,216');
    ObjectSetInteger(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_FONTSIZE, 10);
    ObjectSetString(gs_chartId, BTN_SET_SELL_SIGNAL, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, BTN_SET_SELL_SIGNAL, "Set SELL Signal");
    */
}


//設定標籤文字內容
void SetLabelText(long chartId, string labelName, string labelText) {
    ObjectSetString(chartId, labelName, OBJPROP_TEXT, labelText);
}


//更新資訊標籤內容
void UpdateInfoLabels() {
    SetLabelText(gs_chartId, LBL_PRICE, StringFormat("%." + (string)Digits + "f", Close[0]));
    if(Close[0] > Open[0])
        ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDeepSkyBlue);
    else if(Close[0] < Open[0])
        ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDeepPink);
    else
        ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDarkGray);

    SetLabelText(gs_chartId, LBL_SPREAD, StringFormat("(%.0f)", MarketInfo(gs_symbol, MODE_SPREAD)));

    SetLabelText(gs_chartId, LBL_SERVER_TIME, "主機：" + TimeToString(TimeCurrent(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));
    SetLabelText(gs_chartId, LBL_LOCAL_TIME, "本地：" + TimeToString(TimeLocal(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));
}


//更新趨勢標籤顯示狀態
void UpdateSignalLabel() {
    if(gs_lastSignal == SIGNAL_NONE) {
        SetLabelText(gs_chartId, LBL_SIGNAL, ARROW_NONE);
        ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_COLOR, clrLightGray);

    } else if(gs_lastSignal == SIGNAL_SELL) {
        SetLabelText(gs_chartId, LBL_SIGNAL, ARROW_DOWN);
        ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_COLOR, clrDeepPink);

    } else {
        SetLabelText(gs_chartId, LBL_SIGNAL, ARROW_UP);
        ObjectSetInteger(gs_chartId, LBL_SIGNAL, OBJPROP_COLOR, clrDeepSkyBlue);
    }
}


//控制顯示超過進場時間訊息
void SetTradeTimeLabel(bool isTradeTime) {
    if(isTradeTime) {
        ObjectDelete(gs_chartId, LBL_TRADE_TIME);
            
    } else {
        ObjectCreate(gs_chartId, LBL_TRADE_TIME, OBJ_LABEL, 0, 0, 0);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_CORNER, CORNER_LEFT_UPPER);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_ANCHOR, ANCHOR_LEFT);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_XDISTANCE, 320);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_YDISTANCE, 60);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_COLOR, clrRed);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_FONTSIZE, 24);
        ObjectSetString(gs_chartId, LBL_TRADE_TIME, OBJPROP_FONT, "微軟正黑體");
        SetLabelText(gs_chartId, LBL_TRADE_TIME, TRADE_TIME_MSG);
    }
}


//控制顯示停損暫停交易訊息
void SetStopTradeLabel(bool isStopTrading) {
    if(isStopTrading) {
        ObjectCreate(gs_chartId, LBL_STOP_TRADE, OBJ_LABEL, 0, 0, 0);
        ObjectSetInteger(gs_chartId, LBL_STOP_TRADE, OBJPROP_CORNER, CORNER_LEFT_UPPER);
        ObjectSetInteger(gs_chartId, LBL_STOP_TRADE, OBJPROP_ANCHOR, ANCHOR_LEFT);
        ObjectSetInteger(gs_chartId, LBL_STOP_TRADE, OBJPROP_XDISTANCE, 90);
        ObjectSetInteger(gs_chartId, LBL_STOP_TRADE, OBJPROP_YDISTANCE, 100);
        ObjectSetInteger(gs_chartId, LBL_STOP_TRADE, OBJPROP_COLOR, clrRed);
        ObjectSetInteger(gs_chartId, LBL_STOP_TRADE, OBJPROP_FONTSIZE, 32);
        ObjectSetString(gs_chartId, LBL_STOP_TRADE, OBJPROP_FONT, "微軟正黑體");
        SetLabelText(gs_chartId, LBL_STOP_TRADE, STOP_TRADE_MSG);
            
    } else {
        ObjectDelete(gs_chartId, LBL_STOP_TRADE);
    }
}


//繪製各項關鍵價位線
void DrawTradeLines() {
    if(gs_checkPoint > 0) {
        DrawLine(LN_CHECK_LINE, gs_checkPoint, clrGold, STYLE_DASHDOT);
    } else {
        ObjectDelete(gs_chartId, LN_CHECK_LINE);
    }
    
    if(gs_targetPrice > 0) {
        DrawLine(LN_TARGET_LINE, gs_targetPrice, clrRed, STYLE_DOT);
    } else {
        ObjectDelete(gs_chartId, LN_TARGET_LINE);
    }
}


//繪製線條
void DrawLine(string lineName, double linePrice, long lineColor, int lineStyle = STYLE_SOLID, int lineWidth = 1) {
    ObjectDelete(gs_chartId, lineName);
    ObjectCreate(gs_chartId, lineName, OBJ_HLINE, 0, TimeCurrent(), linePrice);
    ObjectSetInteger(gs_chartId, lineName, OBJPROP_COLOR, lineColor);
    ObjectSetInteger(gs_chartId, lineName, OBJPROP_STYLE, lineStyle);
    ObjectSetInteger(gs_chartId, lineName, OBJPROP_WIDTH, lineWidth);
}