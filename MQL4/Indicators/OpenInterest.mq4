﻿#property copyright "Copyright 2017, Tim Hsu"
#property link      ""
#property version   "2.0"
#property description "OI 籌碼指標"
#property strict
#include <TEA.mqh>
#include <json.mqh>
#include <sqlite.mqh>


//indicator settings
#property indicator_separate_window
#property indicator_buffers 7
#property indicator_plots   3
#property indicator_minimum -5
#property indicator_maximum 5

#property indicator_label1  "OI Change(%)"
#property indicator_type1   DRAW_HISTOGRAM
#property indicator_color1  clrWhite
#property indicator_style1  STYLE_SOLID
#property indicator_width1  3

#property indicator_label2  "OI Trend"
#property indicator_type2   DRAW_ARROW
#property indicator_color2  clrLightSalmon
#property indicator_style2  STYLE_SOLID
#property indicator_width2  2

#property indicator_label3  "OI"
#property indicator_type3   DRAW_NONE
#property indicator_color3  clrWhite
#property indicator_style3  STYLE_SOLID
#property indicator_width3  0

#property indicator_label4  "CME Open"
#property indicator_type4   DRAW_NONE
#property indicator_color4  clrWhite
#property indicator_style4  STYLE_SOLID
#property indicator_width4  0

#property indicator_label5  "CME High"
#property indicator_type5   DRAW_NONE
#property indicator_color5  clrWhite
#property indicator_style5  STYLE_SOLID
#property indicator_width5  0

#property indicator_label6  "CME Low"
#property indicator_type6   DRAW_NONE
#property indicator_color6  clrWhite
#property indicator_style6  STYLE_SOLID
#property indicator_width6  0

#property indicator_label7  "CME Close"
#property indicator_type7   DRAW_NONE
#property indicator_color7  clrWhite
#property indicator_style7  STYLE_SOLID
#property indicator_width7  0

#property indicator_level1  0

const string CME_DB_FILE         = "cme_oi.db";

struct OiDataStruct {
    string currency;
    string tradeDate;
    int monthId;
    string month;
    int openInterest;
    int oiChange;
    double oiChangeRate;
    double open;
    double close;
    double high;
    double low;
};


double ExtOIBuffer[];
double ExtOIChangeBuffer[];
double ExtOITrendBuffer[];
double ExtOpenBuffer[];
double ExtHighBuffer[];
double ExtLowBuffer[];
double ExtCloseBuffer[];

static OiDataStruct gs_oiData[];
static string       gs_lastLoadTime = "";

int OnInit() {
    if(Symbol() != "EURUSD") {
        Alert("Open Interest (OI) 目前僅支援 EURUSD。");
        return INIT_AGENT_NOT_SUITABLE;
    }
    
    if(Period() != PERIOD_D1) {
        Alert("Open Interest (OI) 資料為每日統計，僅可使用於 D1 時區。");
        return INIT_AGENT_NOT_SUITABLE;
    }

    LoadOIData(CME_DB_FILE, Symbol());
    
    IndicatorShortName("CME Open Interest");
    
    IndicatorDigits(5);
    
    SetIndexBuffer(0, ExtOIChangeBuffer);
    SetIndexBuffer(1, ExtOITrendBuffer);
    SetIndexBuffer(2, ExtOIBuffer);
    SetIndexBuffer(3, ExtOpenBuffer);
    SetIndexBuffer(4, ExtHighBuffer);
    SetIndexBuffer(5, ExtLowBuffer);
    SetIndexBuffer(6, ExtCloseBuffer);
    SetIndexArrow(1, 159);
    
    SetIndexLabel(0, "Change(%)");
    SetIndexLabel(1, "OI Trend");
    
    return(INIT_SUCCEEDED);
}


int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {

    //--- counting from 0 to rates_total
    ArraySetAsSeries(ExtOIBuffer, false);
    ArraySetAsSeries(ExtOIChangeBuffer, false);
    ArraySetAsSeries(ExtOITrendBuffer, false);
    ArraySetAsSeries(ExtOpenBuffer, false);
    ArraySetAsSeries(ExtHighBuffer, false);
    ArraySetAsSeries(ExtLowBuffer, false);
    ArraySetAsSeries(ExtCloseBuffer, false);
    ArraySetAsSeries(high, false);
    ArraySetAsSeries(low, false);
    ArraySetAsSeries(time, false);

    int i, limit, oiIdx;
    if(prev_calculated == 0 || (Minute() % 60 == 0 && gs_lastLoadTime != TimeToString(TimeCurrent(), TIME_MINUTES))) {
        gs_lastLoadTime = TimeToString(TimeCurrent(), TIME_MINUTES);
        LoadOIData(CME_DB_FILE, Symbol());

        ExtOIBuffer[0] = 0.0;
        ExtOIChangeBuffer[0] = 0.0;
        ExtOITrendBuffer[0] = 0.0;
        //--- filling out the array by specified file
        oiIdx = 0;
        for(i = 1; i < rates_total; i++) {
            //if(i > ArraySize(gs_oiData) - 1)  break;
            oiIdx = GetOIDataIndex(TimeToString(time[i], TIME_DATE), oiIdx);
            if(oiIdx >= 0) {
                ExtOIBuffer[i] = gs_oiData[oiIdx].openInterest;
                ExtOIChangeBuffer[i] = gs_oiData[oiIdx].oiChangeRate;
                ExtOpenBuffer[i] = gs_oiData[oiIdx].open;
                ExtHighBuffer[i] = gs_oiData[oiIdx].high;
                ExtLowBuffer[i] = gs_oiData[oiIdx].low;
                ExtCloseBuffer[i] = gs_oiData[oiIdx].close;
                
                if(ExtOIChangeBuffer[i] > 0) {  // OI 增
                    if(ExtHighBuffer[i] > 0 && ExtLowBuffer[i] > 0 && ExtHighBuffer[i - 1] > 0 && ExtLowBuffer[i - 1] > 0) { //CME 有價格資料, 優先使用
                        if(ExtHighBuffer[i] > ExtHighBuffer[i - 1] && ExtLowBuffer[i] > ExtLowBuffer[i - 1]) //過高不破低
                            ExtOITrendBuffer[i] = 3;
                        else if(ExtHighBuffer[i] < ExtHighBuffer[i - 1] && ExtLowBuffer[i] < ExtLowBuffer[i - 1]) //破低不過高
                            ExtOITrendBuffer[i] = -3;
                        else
                            ExtOITrendBuffer[i] = 0;
                    
                    } else { //CME 沒價格資料, 就用 K 棒資料
                        if(high[i] > high[i - 1] && low[i] > low[i - 1]) //過高不破低
                            ExtOITrendBuffer[i] = 2;
                        else if(high[i] < high[i - 1] && low[i] < low[i - 1]) //破低不過高
                            ExtOITrendBuffer[i] = -2;
                        else
                            ExtOITrendBuffer[i] = 0;
                    }
                }

                else if(ExtOIChangeBuffer[i] < 0) {  // OI 減
                    if(ExtHighBuffer[i] > 0 && ExtLowBuffer[i] > 0 && ExtHighBuffer[i - 1] > 0 && ExtLowBuffer[i - 1] > 0) { //CME 有價格資料, 優先使用
                        if(ExtHighBuffer[i] > ExtHighBuffer[i - 1] && ExtLowBuffer[i] > ExtLowBuffer[i - 1]) //過高不破低
                            ExtOITrendBuffer[i] = -3;
                        else if(ExtHighBuffer[i] < ExtHighBuffer[i - 1] && ExtLowBuffer[i] < ExtLowBuffer[i - 1]) //破低不過高
                            ExtOITrendBuffer[i] = 3;
                        else
                            ExtOITrendBuffer[i] = 0;
                    
                    } else { //CME 沒價格資料, 就用 K 棒資料
                        if(high[i] > high[i - 1] && low[i] > low[i - 1]) //過高不破低
                            ExtOITrendBuffer[i] = -2;
                        else if(high[i] < high[i - 1] && low[i] < low[i - 1]) //破低不過高
                            ExtOITrendBuffer[i] = 2;
                        else
                            ExtOITrendBuffer[i] = 0;
                    }
                }
                
            } else {
                oiIdx = 0;
            }
        }
    }
    
    return rates_total;
}


void LoadOIData(string dbFile, string currency) {
    //initialize SQLite
    if (!sqlite_init()) {
        Print("Unable to initialize SQLite.");
        return;
    }

    string sqlCmd = "select * from (" +
                    "select oi.TradeDate, oi.Currency, oi.OpenInterest, oi.OiChange, p.Open, p.High, p.Low, p.Close " +
                    "from (select TradeDate, Currency, OpenInterest, OiChange from CmeOi where Month = 'total' and currency = '" + currency + "') oi " +
                    "join (select TradeDate, Currency, Open, High, Low, Close from CmeOi where MonthID = 1 and currency = '" + currency + "') p " +
                    "on oi.TradeDate = p.TradeDate and oi.Currency = p.Currency " +
                    "order by oi.TradeDate)";

    int cols[1];
    int handle = sqlite_query(dbFile, sqlCmd, cols);
    ArrayFree(gs_oiData);
    int idx = 0;
    int lastIdx = 0;
    while (sqlite_next_row(handle) == 1) {
        if(idx >= lastIdx) {
            ArrayResize(gs_oiData, lastIdx + 20, 25);
            lastIdx = ArraySize(gs_oiData) - 1;
        }
        
        gs_oiData[idx].tradeDate = sqlite_get_col(handle, 0);
        gs_oiData[idx].currency = sqlite_get_col(handle, 1);
        gs_oiData[idx].openInterest = StringToInteger(sqlite_get_col(handle, 2));
        gs_oiData[idx].oiChange = StringToInteger(sqlite_get_col(handle, 3));
        if(idx > 0) {
            gs_oiData[idx].oiChangeRate = ((double)gs_oiData[idx].oiChange / (double)gs_oiData[idx - 1].openInterest) * 100;
        }
        gs_oiData[idx].open = StringToDouble(sqlite_get_col(handle, 4));
        gs_oiData[idx].high = StringToDouble(sqlite_get_col(handle, 5));
        gs_oiData[idx].low = StringToDouble(sqlite_get_col(handle, 6));
        gs_oiData[idx].close = StringToDouble(sqlite_get_col(handle, 7));
        
        idx++;
    }

    sqlite_free_query (handle);

/*    
    int file = FileOpen(OI_RECORDS_FILE, FILE_COMMON | FILE_SHARE_READ );
    if(file != INVALID_HANDLE) {
        
        ArrayResize(gs_oiData, 20, 25);
        int lastIdx = ArraySize(gs_oiData) - 1;
        int idx = 0;
        string line;
        string tmp[];
        while(!FileIsEnding(file)) {
            line = StringTrimLeft(StringTrimRight(FileReadString(file)));
            StringSplit(line, ',', tmp);
            if(tmp[0] == "Currency")  continue;
            if(tmp[0] != _Symbol)  continue;
            if(ArraySize(tmp) != 9) {
                Print("fields are not expected. ", line);
                continue;
            }
            
            gs_oiData[idx].currency = tmp[0];
            gs_oiData[idx].tradeDate = tmp[1];
            gs_oiData[idx].openInterest = StringToInteger(tmp[3]);
            gs_oiData[idx].oiChange = StringToInteger(tmp[4]);
            if(idx > 0) {
                gs_oiData[idx].oiChangeRate = ((double)gs_oiData[idx].oiChange / (double)gs_oiData[idx - 1].openInterest) * 100;
            }
            gs_oiData[idx].open = StringToDouble(tmp[5]);
            gs_oiData[idx].high = StringToDouble(tmp[6]);
            gs_oiData[idx].low = StringToDouble(tmp[7]);
            gs_oiData[idx].close = StringToDouble(tmp[8]);
            
            idx++;
            if(idx >= lastIdx) {
                ArrayResize(gs_oiData, lastIdx + 20, 25);
                lastIdx = ArraySize(gs_oiData) - 1;
            }
        }
        
        FileClose(file);
    }
*/
    sqlite_finalize();
}


int GetOIDataIndex(string tradeDate, int startIdx = 0) {
    for(int i = startIdx; i < ArraySize(gs_oiData); i++) {
        if(gs_oiData[i].tradeDate == Replace(tradeDate, ".", "")) {
            return i;
        }   
    }
    
    return -1;
}