#property copyright "Copyright 2017, Tim Hsu"
#property link      ""
#property version   "1.02"
#property description "Tim's Donchain Channel Indicator"
#property strict

#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2
//Upper
#property indicator_label1  "Upper"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrDodgerBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  2
//Lower
#property indicator_label2  "Lower"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrDarkOrange
#property indicator_style2  STYLE_SOLID
#property indicator_width2  2

input int PERIODS = 20;     //區間範圍
input int SHIFTS  = 1;      //平移量

double UpperBuffer[];
double LowerBuffer[];


int OnInit() {
    IndicatorShortName(StringFormat("Donchain(%d,%d)", PERIODS, SHIFTS));
    IndicatorDigits(Digits());
    
    SetIndexBuffer(0, UpperBuffer);
    SetIndexLabel(0, StringFormat("Upper(%d,%d)", PERIODS, SHIFTS));
    
    SetIndexBuffer(1, LowerBuffer);
    SetIndexLabel(1, StringFormat("Lower(%d,%d)", PERIODS, SHIFTS));

    SetIndexDrawBegin(0, PERIODS + SHIFTS);
    SetIndexDrawBegin(1, PERIODS + SHIFTS);
    
    return(INIT_SUCCEEDED);
}


int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {
   
    int limit = rates_total - prev_calculated;
    if(prev_calculated > 0) limit++;
    
    for(int i = 0; i < limit; i++) {
        UpperBuffer[i] = iHigh(Symbol(), Period(), iHighest(Symbol(), Period(), MODE_HIGH, PERIODS, i + SHIFTS));
        LowerBuffer[i] = iLow(Symbol(), Period(), iLowest(Symbol(), Period(), MODE_LOW, PERIODS, i + SHIFTS));
    }
    
    return rates_total;
}
