package com.timhsu.forextools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Set;

import com.timhsu.forextools.commands.Command;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.reflections.Reflections;

public class ForexTool {
    public static final String CAYENNE_ORM_XML = "cayenne-ForexTools.xml";
    public static final String CONFIG_FILE = "forextools.config";
    private static Properties propBag;

    static {
        try {
            propBag = new Properties();
            InputStreamReader reader = new InputStreamReader(new FileInputStream(CONFIG_FILE), "UTF-8");
            propBag.load(reader);
        } catch (IOException e) {
            System.out.println("Failed to load config file. " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        Reflections reflections = new Reflections();
        Set<Class<? extends Command>> commands = reflections.getSubTypesOf(Command.class);

        if (args.length == 0) {
            printAvailableCommands(commands);
            System.out.println();
            System.exit(-1);
        }

        for (Class<? extends Command> command : commands) {
            String cmd = command.getSimpleName().toLowerCase();
            if (cmd.equals(args[0].toLowerCase())) {
                String commandArgs[] = new String[args.length - 1];
                for (int i = 0; i < commandArgs.length; i++) {
                    commandArgs[i] = args[i + 1];
                }
                int exitCode = 0;
                try {
                    exitCode = command.newInstance().run(commandArgs);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println();
                    System.exit(-1);
                }
                System.out.println();
                System.exit(exitCode);
            }
        }

        printAvailableCommands(commands);
        System.out.println();
        System.exit(-1);
    }

    private static void printAvailableCommands(Set<Class<? extends Command>> commands) {
        System.out.println("Usage: "
                + ForexTool.class.getSimpleName() + " {Command}\n");
        System.out.println("Available Command(s):\n");
        for (Class<? extends Command> command : commands) {
            String cmd = command.getSimpleName();
            System.out.println(cmd);
        }
    }

    public static ObjectContext getCayenneContext() {
        ServerRuntime cayenneRuntime = new ServerRuntime(CAYENNE_ORM_XML);
        return cayenneRuntime.getContext();
    }

    public static String getProperty(String propertyKey, String defaultValue) {
        return propBag.getProperty(propertyKey, defaultValue);
    }
}
