package com.timhsu.forextools.commands;

public interface Command {
    public int run(String args[]) throws Exception;
}
