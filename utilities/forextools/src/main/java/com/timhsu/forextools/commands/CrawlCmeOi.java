package com.timhsu.forextools.commands;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.timhsu.forextools.ForexTool;
import com.timhsu.forextools.persistent.CmeOi;
import org.apache.cayenne.Cayenne;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class CrawlCmeOi implements Command {
    //http://www.cmegroup.com/CmeWS/mvc/Volume/Details/F/58/20170228/F?tradeDate=20170228
    private static final String CME_OI_END_POINT = "http://www.cmegroup.com/CmeWS/mvc/Volume/Details/F/%d/%s/F?tradeDate=%s";
    //http://www.cmegroup.com/CmeWS/mvc/Settlements/Futures/Settlements/58/FUT?strategy=DEFAULT&tradeDate=02/28/2017
    private static final String CME_PRICE_END_POINT = "http://www.cmegroup.com/CmeWS/mvc/Settlements/Futures/Settlements/%d/FUT?strategy=DEFAULT&tradeDate=%s";

    private int crawlingDays = Integer.parseInt(ForexTool.getProperty("CrawlCmeOi.CrawlingDays", "10"));
    private String oiDataType = ForexTool.getProperty("CrawlCmeOi.OiDataType", "Nearest");
    private String currency = ForexTool.getProperty("CrawlCmeOi.Currency", "EURUSD");
    private String outputFile = ForexTool.getProperty("MT4.SharedFolder", ".") +
            File.separator +
            ForexTool.getProperty("CrawlCmeOi.OutputFile", "CME_OI.csv");

    public int run(String[] args) throws Exception {
        if (args.length > 0 && args[0].toUpperCase().equals("HELP")) {
            System.out.println("Usage: "
                    + ForexTool.class.getSimpleName() + " "
                    + CrawlCmeOi.class.getSimpleName() + " <CurrencyCode> <Output.csv> [<StartDate> <EndDate>]");
            System.out.println("");
            return 0;
        }

        if (args.length > 0) currency = args[0].toUpperCase();
        if (args.length > 1) outputFile = ForexTool.getProperty("MT4.SharedFolder", "")
                + File.separator
                + args[1];

        try {
            Calendar startDate;
            Calendar endDate;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            if (args.length == 4) {
                Date start = sdf.parse(args[2]);
                startDate = Calendar.getInstance();
                startDate.setTime(start);
                Date end = sdf.parse(args[3]);
                endDate = Calendar.getInstance();
                endDate.setTime(end);

            } else {
                startDate = Calendar.getInstance();
                startDate.add(Calendar.DAY_OF_MONTH, -1 * crawlingDays);
                endDate = Calendar.getInstance();
            }

            String dateToCrawl = sdf.format(startDate.getTime());
            String endDateToCrawl = sdf.format(endDate.getTime());

            while (Integer.parseInt(dateToCrawl) <= Integer.parseInt(endDateToCrawl)) {
                String oiData = crawlCmeOi(currency, dateToCrawl);
                String oiPrice = crawlCmePrice(currency, dateToCrawl);
                parseOiData(oiData, oiPrice, currency);

                startDate.add(Calendar.DAY_OF_MONTH, 1);
                dateToCrawl = sdf.format(startDate.getTime());
            }
            exportOiData(outputFile);

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

        return 0;
    }

    public String crawlCmeOi(String currency, String dateToCrawl) throws Exception {
        URL cmeEndPoint = new URL(String.format(CME_OI_END_POINT, getCurrencyCode(currency), dateToCrawl, dateToCrawl));
        HttpURLConnection conn = (HttpURLConnection) cmeEndPoint.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder resp = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            resp.append(line);
        }
        in.close();

        return resp.toString();
    }

    public String crawlCmePrice(String currency, String dateToCrawl) throws Exception {
        String dateToCrawl2 = dateToCrawl.substring(4, 6) + "/" + dateToCrawl.substring(6, 8) + "/" + dateToCrawl.substring(0, 4);
        URL cmeEndPoint = new URL(String.format(CME_PRICE_END_POINT, getCurrencyCode(currency), dateToCrawl2));
        HttpURLConnection conn = (HttpURLConnection) cmeEndPoint.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder resp = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            resp.append(line);
        }
        in.close();

        return resp.toString();
    }

    public int getCurrencyCode(String currency) {
        if (currency.toUpperCase().equals("EURUSD")) return 58;
        if (currency.toUpperCase().equals("USDJPY")) return 69;

        return -1;
    }

    public void parseOiData(String oiData, String priceData, String currency) {
        JsonObject oiJson = new JsonParser().parse(oiData).getAsJsonObject();
        if (oiJson.get("empty").getAsBoolean()) {
            return;
        }
        JsonObject priceJson = new JsonParser().parse(priceData).getAsJsonObject();
        if (priceJson.get("empty").getAsBoolean()) {
            return;
        }

        ArrayList<OiData> allData = new ArrayList<OiData>();

        //parse open interest
        String tradeDate = oiJson.get("tradeDate").getAsString();
        JsonObject totals = oiJson.get("totals").getAsJsonObject();
        int totalAtClose = Integer.parseInt(totals.get("atClose").getAsString().replace(",", ""));
        int totalChange = Integer.parseInt(totals.get("change").getAsString().replace(",", ""));
        allData.add(new OiData(currency, tradeDate, 0, "total", totalAtClose, totalChange));

        JsonArray monthData = oiJson.get("monthData").getAsJsonArray();
        for (int i = 0; i < monthData.size(); i++) {
            JsonObject o = monthData.get(i).getAsJsonObject();
            allData.add(new OiData(
                    currency,
                    tradeDate,
                    i + 1,
                    o.get("month").getAsString(),
                    Integer.parseInt(o.get("atClose").getAsString().replace(",", "")),
                    Integer.parseInt(o.get("change").getAsString().replace(",", ""))
            ));
        }

        ObjectContext context = ForexTool.getCayenneContext();
        for (OiData d : allData) {
            SelectQuery qry = new SelectQuery(CmeOi.class);
            qry.setQualifier(ExpressionFactory.matchExp(CmeOi.CURRENCY_PROPERTY, currency));
            qry.andQualifier(ExpressionFactory.matchExp(CmeOi.TRADE_DATE_PROPERTY, d.tradeDate));
            qry.andQualifier(ExpressionFactory.matchExp(CmeOi.MONTH_ID_PROPERTY, d.monthId));
            qry.andQualifier(ExpressionFactory.matchExp(CmeOi.MONTH_PROPERTY, d.month));
            CmeOi oi = (CmeOi) Cayenne.objectForQuery(context, qry);

            if (oi == null) {
                oi = context.newObject(CmeOi.class);
            }

            oi.setCurrency(currency);
            oi.setTradeDate(d.tradeDate);
            oi.setMonth(d.month);
            oi.setMonthID(d.monthId);
            oi.setOpenInterest(d.openInterest);
            oi.setOiChange(d.oiChange);
        }

        context.commitChanges();

        //merge price data
        allData = new ArrayList<OiData>();
        tradeDate = priceJson.get("tradeDate").getAsString();
        tradeDate = tradeDate.substring(6, 10) + tradeDate.substring(0, 2) + tradeDate.substring(3, 5);
        JsonArray settlements = priceJson.get("settlements").getAsJsonArray();
        for (int i = 0; i < settlements.size(); i++) {
            JsonObject o = settlements.get(i).getAsJsonObject();
            if (o.get("month").getAsString().equals("Total")) continue;

            allData.add(new OiData(
                    currency,
                    tradeDate,
                    o.get("month").getAsString(),
                    Double.parseDouble(o.get("open").getAsString().replace("A", "").replace("B", "").replace("-", "0")),
                    Double.parseDouble(o.get("last").getAsString().replace("A", "").replace("B", "").replace("-", "0")),
                    Double.parseDouble(o.get("high").getAsString().replace("A", "").replace("B", "").replace("-", "0")),
                    Double.parseDouble(o.get("low").getAsString().replace("A", "").replace("B", "").replace("-", "0"))
            ));
        }

        for (OiData d : allData) {
            SelectQuery qry = new SelectQuery(CmeOi.class);
            qry.setQualifier(ExpressionFactory.matchExp(CmeOi.CURRENCY_PROPERTY, currency));
            qry.andQualifier(ExpressionFactory.matchExp(CmeOi.TRADE_DATE_PROPERTY, d.tradeDate));
            qry.andQualifier(ExpressionFactory.matchExp(CmeOi.MONTH_PROPERTY, d.month));
            CmeOi oi = (CmeOi) Cayenne.objectForQuery(context, qry);

            if (oi == null) {
                oi = context.newObject(CmeOi.class);
                oi.setCurrency(currency);
                oi.setTradeDate(d.tradeDate);
                oi.setMonth(d.month);
            }

            oi.setOpen(d.open);
            oi.setClose(d.close);
            oi.setHigh(d.high);
            oi.setLow(d.low);
        }

        context.commitChanges();
    }

    public void exportOiData(String outputFile) throws IOException {
        FileWriter fw = new FileWriter(outputFile);
        CSVPrinter csvWriter = new CSVPrinter(fw, CSVFormat.DEFAULT);
        ObjectContext context = ForexTool.getCayenneContext();
        String sqlCmd = "";
        if (oiDataType.toUpperCase().equals("NEAREST")) {
            sqlCmd = "select * from CmeOi where MonthID = 1 order by TradeDate";
        } else if (oiDataType.toUpperCase().equals("TOTAL")) {
            sqlCmd = "select * from (" +
                    "select p.row_id, oi.TradeDate, oi.Currency, p.MonthID, p.Month, p.Open, p.High, p.Low, p.Close, oi.OpenInterest, oi.OiChange " +
                    "from (select TradeDate, Currency, OpenInterest, OiChange from CmeOi where MonthID = 0) oi " +
                    "join (select TradeDate, Currency, MonthID, Month, Open, High, Low, Close, row_id from CmeOi where MonthID = 1) p " +
                    "on oi.TradeDate = p.TradeDate and oi.Currency = p.Currency " +
                    "order by oi.TradeDate)";
        }
        SQLTemplate sql = new SQLTemplate(CmeOi.class, sqlCmd);
//        sql.setFetchLimit(1000);
//        SelectQuery qry = new SelectQuery(CmeOi.class);
//        qry.setQualifier(ExpressionFactory.matchExp(CmeOi.MONTH_ID_PROPERTY, 1));
//        qry.addOrdering("tradeDate", SortOrder.ASCENDING);

        List<CmeOi> oiData = context.performQuery(sql);

        csvWriter.printRecord("Currency", "TradeDate", "Month", "OI", "OIChange", "Open", "High", "Low", "Close");
        for (CmeOi o : oiData) {
            csvWriter.printRecord(
                    o.getCurrency(),
                    o.getTradeDate().substring(0, 4) + "." + o.getTradeDate().substring(4, 6) + "." + o.getTradeDate().substring(6, 8),
                    o.getMonth(),
                    o.getOpenInterest(),
                    o.getOiChange(),
                    o.getOpen(),
                    o.getHigh(),
                    o.getLow(),
                    o.getClose()
            );
        }

        csvWriter.flush();
        csvWriter.close();
        fw.close();
    }

    private class OiData {
        public String currency;
        public String tradeDate;
        public int monthId;
        public String month;
        public int openInterest;
        public int oiChange;
        public double open;
        public double close;
        public double high;
        public double low;

        public OiData(String currency, String tradeDate, int monthId, String month, int openInterest, int oiChange) {
            this.currency = currency;
            this.tradeDate = tradeDate;
            this.monthId = monthId;
            this.month = month;
            this.openInterest = openInterest;
            this.oiChange = oiChange;
        }

        public OiData(String currency, String tradeDate, String month, double open, double close, double high, double low) {
            this.currency = currency;
            this.tradeDate = tradeDate;
            this.month = month;
            this.open = open;
            this.close = close;
            this.high = high;
            this.low = low;
        }
    }
}
