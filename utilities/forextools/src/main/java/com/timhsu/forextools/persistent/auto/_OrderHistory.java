package com.timhsu.forextools.persistent.auto;

import org.apache.cayenne.CayenneDataObject;

/**
 * Class _OrderHistory was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _OrderHistory extends CayenneDataObject {

    public static final String ACCOUNT_PROPERTY = "account";
    public static final String CLOSE_DATE_PROPERTY = "closeDate";
    public static final String CLOSE_PRICE_PROPERTY = "closePrice";
    public static final String CLOSE_TIME_PROPERTY = "closeTime";
    public static final String COMMENT_PROPERTY = "comment";
    public static final String COMMISSION_PROPERTY = "commission";
    public static final String LOTS_PROPERTY = "lots";
    public static final String MAGIC_NUMBER_PROPERTY = "magicNumber";
    public static final String OPEN_DATE_PROPERTY = "openDate";
    public static final String OPEN_PRICE_PROPERTY = "openPrice";
    public static final String OPEN_TIME_PROPERTY = "openTime";
    public static final String ORDER_TYPE_PROPERTY = "orderType";
    public static final String PROFIT_PROPERTY = "profit";
    public static final String STOP_LOSS_PROPERTY = "stopLoss";
    public static final String SWAP_PROPERTY = "swap";
    public static final String SYMBOL_PROPERTY = "symbol";
    public static final String TAKE_PROFIT_PROPERTY = "takeProfit";
    public static final String TICKET_PROPERTY = "ticket";

    public static final String ROW_ID_PK_COLUMN = "row_id";

    public void setAccount(String account) {
        writeProperty(ACCOUNT_PROPERTY, account);
    }
    public String getAccount() {
        return (String)readProperty(ACCOUNT_PROPERTY);
    }

    public void setCloseDate(String closeDate) {
        writeProperty(CLOSE_DATE_PROPERTY, closeDate);
    }
    public String getCloseDate() {
        return (String)readProperty(CLOSE_DATE_PROPERTY);
    }

    public void setClosePrice(Double closePrice) {
        writeProperty(CLOSE_PRICE_PROPERTY, closePrice);
    }
    public Double getClosePrice() {
        return (Double)readProperty(CLOSE_PRICE_PROPERTY);
    }

    public void setCloseTime(String closeTime) {
        writeProperty(CLOSE_TIME_PROPERTY, closeTime);
    }
    public String getCloseTime() {
        return (String)readProperty(CLOSE_TIME_PROPERTY);
    }

    public void setComment(String comment) {
        writeProperty(COMMENT_PROPERTY, comment);
    }
    public String getComment() {
        return (String)readProperty(COMMENT_PROPERTY);
    }

    public void setCommission(Double commission) {
        writeProperty(COMMISSION_PROPERTY, commission);
    }
    public Double getCommission() {
        return (Double)readProperty(COMMISSION_PROPERTY);
    }

    public void setLots(Double lots) {
        writeProperty(LOTS_PROPERTY, lots);
    }
    public Double getLots() {
        return (Double)readProperty(LOTS_PROPERTY);
    }

    public void setMagicNumber(Integer magicNumber) {
        writeProperty(MAGIC_NUMBER_PROPERTY, magicNumber);
    }
    public Integer getMagicNumber() {
        return (Integer)readProperty(MAGIC_NUMBER_PROPERTY);
    }

    public void setOpenDate(String openDate) {
        writeProperty(OPEN_DATE_PROPERTY, openDate);
    }
    public String getOpenDate() {
        return (String)readProperty(OPEN_DATE_PROPERTY);
    }

    public void setOpenPrice(Double openPrice) {
        writeProperty(OPEN_PRICE_PROPERTY, openPrice);
    }
    public Double getOpenPrice() {
        return (Double)readProperty(OPEN_PRICE_PROPERTY);
    }

    public void setOpenTime(String openTime) {
        writeProperty(OPEN_TIME_PROPERTY, openTime);
    }
    public String getOpenTime() {
        return (String)readProperty(OPEN_TIME_PROPERTY);
    }

    public void setOrderType(Integer orderType) {
        writeProperty(ORDER_TYPE_PROPERTY, orderType);
    }
    public Integer getOrderType() {
        return (Integer)readProperty(ORDER_TYPE_PROPERTY);
    }

    public void setProfit(Double profit) {
        writeProperty(PROFIT_PROPERTY, profit);
    }
    public Double getProfit() {
        return (Double)readProperty(PROFIT_PROPERTY);
    }

    public void setStopLoss(Double stopLoss) {
        writeProperty(STOP_LOSS_PROPERTY, stopLoss);
    }
    public Double getStopLoss() {
        return (Double)readProperty(STOP_LOSS_PROPERTY);
    }

    public void setSwap(Double swap) {
        writeProperty(SWAP_PROPERTY, swap);
    }
    public Double getSwap() {
        return (Double)readProperty(SWAP_PROPERTY);
    }

    public void setSymbol(String symbol) {
        writeProperty(SYMBOL_PROPERTY, symbol);
    }
    public String getSymbol() {
        return (String)readProperty(SYMBOL_PROPERTY);
    }

    public void setTakeProfit(Double takeProfit) {
        writeProperty(TAKE_PROFIT_PROPERTY, takeProfit);
    }
    public Double getTakeProfit() {
        return (Double)readProperty(TAKE_PROFIT_PROPERTY);
    }

    public void setTicket(Integer ticket) {
        writeProperty(TICKET_PROPERTY, ticket);
    }
    public Integer getTicket() {
        return (Integer)readProperty(TICKET_PROPERTY);
    }

}
